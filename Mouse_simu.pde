int flug1 = 0; //color flag
int step = 1; //init mou11se
String lin; //result_buff
int ln; //iterator
String lines[]; //two_file_buff
int[] data = {};
String buf; //data_buff
int[] x = {}; //result_x
int[] y = {}; //result_y

void setup() {
  //init map
  frameRate(10); //speed = 1/8
  size(540, 540);
  colorMode(RGB, 256);
  background(255, 255, 255); //background color
  ln=0;
  lines = loadStrings("maze_result.txt"); //file_read
  while (true) {
    lin =lines[ln];
    String[] co = split(lin, ',');
    if (co.length==2) {
      //print(int(co[0]),int(co[1]),"\n");
      x = append(x, int(co[0])); //dynamic array
      y = append(y, int(co[1]));
      print(x[ln], y[ln], "\n");
    }
    ln++;
    if (ln == lines.length) {
      break;
    }
  }
  x = append(x, unhex("FFFF")); //append null
  y = append(y, unhex("FFFF"));
  ln=0;
  lines = loadStrings("maze.txt");
  while (true) {
    buf =lines[ln];
    String[] co = split(buf, 'x'); //string2hex
    if (co.length==2) {
      data = append(data, unhex(co[1]));
      print(data[ln], "\n");
    }
    ln++;
    if (ln == lines.length) {
      break;
    }
  }
  for (int j = 0; j < 16; j++) {
    for (int i = 0; i < 16; i++) {
      noStroke();
      if (((i == 7) || (i == 8)) && ((j == 7) || (j == 8)) ) { //central color set
        fill(255, 255, 0);
        rect(30 + 30 * i, 30 + 30 * j, 28, 28);
      } else { //other color set
        fill(0, 0, 0);
        rect(30 + 30 * i, 30 + 30 * j, 28, 28);
      }
    }
  }
  for (int j = 0; j < 16; j++) { //length wall preset
    for (int i = 0; i < 17; i++) {
      if (i<16) {
        if ((data[i+j*16]&1)==1) {
          noStroke();
          fill(255, 160, 167);
          rect(28 + 30 * (i+1), 28 + 30 * (15-j), 2, 32);
        }
      }
    }
  }
  for (int j = 0; j < 16; j++) { //around length wall set
    for (int i = 0; i < 17; i++) {
      if ((i == 0) || (i == 16)) {
        noStroke();
        fill(255, 0, 0);
        rect(28 + 30 * i, 28 + 30 * j, 2, 32);
      }
    }
  }
  for (int j = 0; j < 17; j++) { //width wall preset
    for (int i = 0; i < 16; i++) {
      if (j<16) {
        if ((data[i+j*16]&2)==2) {
          noStroke();
          fill(255, 160, 167);
          rect(28 + 30 * i, 28 + 30 * (15-j), 32, 2);
        }
      }
    }
  }
  for (int j = 0; j < 17; j++) { //around width wall set
    for (int i = 0; i < 16; i++) {
      if ((j == 0) || (j == 16)) {
        noStroke();
        fill(255, 0, 0);
        rect(28 + 30 * i, 28 + 30 * j, 32, 2);
      }
    }
  }
}
int i = 0;
int temp = 0;
int flag = 0;
void draw() {
  noStroke();
  if (flag == 1) { //return flag = on
    fill(250, 50, 0);
    rect(37 + 30 * x[i], 37 + 30 * (15-y[i]), 14, 14);
    if (i>0) {
      fill(50, 250, 0);
      rect(37 + 30 * x[i-1], 37 + 30 * (15-y[i-1]), 14, 14);
    }
  }
  else { //return flag = off
    fill(0, 250, 50);
    rect(30 + 30 * x[i], 30 + 30 * (15-y[i]), 28, 28);
    if (i>0) {
      fill(0, 50, 250);
      rect(30 + 30 * x[i-1], 30 + 30 * (15-y[i-1]), 28, 28);
    }
  }
  if ((x[i]>6)&&(x[i]<9)&&(y[i]>6)&&(y[i]<9)) {
    flag=1;
  }
  if (((x[i]==0)&&(y[i]==0))/*||((x[i]==0)&&(y[i]==1))*/) {
    flag=0;
    temp++;
  }
  if ((data[x[i]+y[i]*16]&1)==1) {//set wall
    noStroke();
    fill(255, 0, 0);
    rect(28 + 30 * (x[i]+1), 28 + 30 * (15-y[i]), 2, 32);
  }
  if (x[i]>0) { //set wall
    if ((data[x[i]-1+y[i]*16]&1)==1) {
      noStroke();
      fill(255, 0, 0);
      rect(28 + 30 * x[i], 28 + 30 * (15-y[i]), 2, 32);
    }
  }
  if ((data[x[i]+y[i]*16]&2)==2) {//set wall
    noStroke();
    fill(255, 0, 0);
    rect(28 + 30 * x[i], 28 + 30 * (15-y[i]), 32, 2);
  }
  if (y[i]>0) {//set wall
    if ((data[x[i]+(y[i]-1)*16]&2)==2) {
      noStroke();
      fill(255, 0, 0);
      rect(28 + 30 * x[i], 28 + 30 * (15-y[i]+1), 32, 2);
    }
  }
  i++;
  if ((x[i] == unhex("FFFF"))||(y[i] == unhex("FFFF"))) {
    print(i);
    noLoop();
  }
}
